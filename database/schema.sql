/* todo:
 *  Для PostreSQL оба запроса не сработают:
 *  AUTO_INCREMENT и Engine=InnoDB нужно убрать.
 *  id INT заменить на id SERIAL.
 *  created_at DATETIME заменить на created_at timestamp
 */

CREATE TABLE project (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    title VARCHAR(255) NOT NULL,
    created_at DATETIME DEFAULT CURRENT_TIMESTAMP
) Engine=InnoDB;

CREATE TABLE task (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    project_id INT NOT NULL,
    title VARCHAR(255) NOT NULL,
    status VARCHAR(16) NOT NULL,
    created_at DATETIME DEFAULT CURRENT_TIMESTAMP
) Engine=InnoDB;
