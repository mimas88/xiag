<?php

namespace App\Storage;

use App\Model;

class DataStorage
{
    /**
     * @var \PDO
     * todo: область видимости стоит сделать protected
     */
    public $pdo;

    public function __construct()
    {   /* todo: Раз подключаются компоненты Symfony, то неплохой вариант был бы использовать Doctrine, вместо PDO.
         *  Но раз PDO, то про него напишу:
         *  При каждом создании объекта этого класса, будет создаваться новое соединение, что не хорошо.
         *  Поэтому весь класс стоит сделать Singleton'ом. Если подумать на будущее, то лучше бы разбить класс на несколько
         *  хотя бы на 2 - Соединение с БД отдельно, а запросы отдельно, иначе через какое-то время класс будет перегружен методами,
         *  ещё нарушается принцип единой ответственности.
         *  Строку dsn стоит вынести в некий конфигурационный файл, чтобы было удобно при deploy'е и тестировании настраивать окружение.
         *  Если всё же PostreSQL, то заменить mysql на pgsql в dsn.
         */
        $this->pdo = new \PDO('mysql:dbname=task_tracker;host=127.0.0.1', 'user');
    }

    /**
     * @param int $projectId
     * @throws Model\NotFoundException
     */
    public function getProjectById($projectId)
    {
        /* todo: нельзя подставлять переменные напрямую в запрос, возможны SQL-инъекции.
         *  Нужно использовать подготовленные выражения или привязку значений (bindValue).
         *  $stmt = $this->pdo->query('SELECT * FROM project WHERE id = :id');
         *  $stmt->execute(array('id' => (int) $projectId));
         */
        $stmt = $this->pdo->query('SELECT * FROM project WHERE id = ' . (int) $projectId);

        if ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            return new Model\Project($row);
        }

        throw new Model\NotFoundException();
    }

    /**
     * @param int $project_id
     * @param int $limit
     * @param int $offset
     */
    public function getTasksByProjectId(int $project_id, $limit, $offset)
    {
        /* todo: вместо ? в подготовленных выражениях стоит использовать именнованные плейсхолдеры для однообразия.
         *  когда мало параметров это не выглядит проблеммой, но на запросах с большим количеством параметров очень неудобно
         *  читать код и высчитывать порядок нужного параметра.
         *  Но в данном случае код работать не будет, поскольку при подстановке, числа будут заключены в кавычки
         *  (примерно так LIMIT '10', '0').
         *  Поэтому нужно исправить на:
         *  $stmt->bindValue(1, $limit, \PDO::PARAM_INT);
         *  $stmt->bindValue(2, $offset, \PDO::PARAM_INT);
         *  $stmt->execute();
         *  и задать значения по умолчанию (в README.md они есть 10 и 0)
         *
         */
        $stmt = $this->pdo->query("SELECT * FROM task WHERE project_id = $project_id LIMIT ?, ?");
        $stmt->execute([$limit, $offset]);

        $tasks = [];
        foreach ($stmt->fetchAll() as $row) {
            $tasks[] = new Model\Task($row);
        }

        return $tasks;
    }

    /**
     * @param array $data
     * @param int $projectId
     * @return Model\Task
     */
    public function createTask(array $data, $projectId)
    {
        $data['project_id'] = $projectId;
        /* todo: Нужно сделать белый лист разрешённых полей и по нему отфильтровать переданные поля.
         *  Для этого в классе Task можно сделать метод fields().
         *  Значения нужно проверить на корректность. Хорошо бы использовать специальные validator'ы.
         *  Но если без них, то хотя бы через bindValue(), как-то так:
         *  function pdoSet($allowed, &$values, $source = array()) {
         *    $set = '';
         *    $values = array();
         *    if (empty($source)) {
         *      $source = $_POST;
         *    }
         *    foreach ($allowed as $field) {
         *       if (isset($source[$field])) {
         *         $set.=":$field, ";
         *         if(is_integer($source[$field])) {
         *           $values[$field] = [
         *                              'type' => \PDO::PARAM_INT,
         *                              'value' => $source[$field]
         *                             ];
         *         } else {
         *           $values[$field] = [
         *                              'type' => \PDO::PARAM_STRING,
         *                              'value' => $source[$field]
         *                             ];;
         *         }
         *       }
         *     }
         *    return substr($set, 0, -2);
         *  }
         *  $allowed = Model\Task::fields(); // allowed fields
         *  $sql = "INSERT INTO users ($allowed) VALUES (" . pdoSet($allowed, $values, $data) . ")";
         *  $stmt = $dbh->prepare($sql);
         *  foreach($values as $nameField => $paramField) {
         *    $stmt->bindValue($nameField, $paramField['value'], $paramField['type']);
         *  }
         *  $stmt->execute();
         */
        $fields = implode(',', array_keys($data));
        $values = implode(',', array_map(function ($v) {
            return is_string($v) ? '"' . $v . '"' : $v;
        }, $data));

        $this->pdo->query("INSERT INTO task ($fields) VALUES ($values)");
        // todo: Для получения id последней вставленной записи, нужно использовать pdo->lastInsertId().
        $data['id'] = $this->pdo->query('SELECT MAX(id) FROM task')->fetchColumn();

        return new Model\Task($data);
    }
}
