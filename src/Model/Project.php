<?php

namespace App\Model;

class Project
{
    /**
     * @var array
     * todo: Подчёркивание запрещается в PSR (хотя на данный момент этот PSR является устаревшим).
     *  На практике использовал подчёркивание для private свойств, немного увеличивает читаемость кода.
     *  Однако, Project является детерминированной сущностью, которая имеет чёткое представление в БД.
     *  Поэтому заменил бы свойство $_data, свойствами $id, $title, $created_at с областями видимости protected
     *  и добавил бы сеттеры и геттеры для этих свойств.
     */
    public $_data;
    
    public function __construct($data)
    {   // todo: через сеттеры наполнял бы объект данными
        $this->_data = $data;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return (int) $this->_data['id'];
    }

    /**
     * @return string
     */
    public function toJson()
    {   /* todo: для однообразия с Task использовал бы интерфейс \JsonSerializable и метод jsonSerialize()
         *  возвращал бы массив, сформированный через геттеры
        */
        return json_encode($this->_data);
    }
}
