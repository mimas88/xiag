<?php

namespace App\Model;

class Task implements \JsonSerializable
{
    /**
     * @var array
     * todo: Аналогично классу Project, заменил бы свойство $_data, свойствами $id, $project_id, $title,
     *  $status, $created_at с областями видимости protected и добавил бы сеттеры и геттеры для этих свойств.
     */
    private $_data;
    
    public function __construct($data)
    {
        // todo: через сеттеры наполнял бы объект данными
        $this->_data = $data;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {   // todo: возвращал бы массив, сформированный через геттеры
        return $this->_data;
    }
}
