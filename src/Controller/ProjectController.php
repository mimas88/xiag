<?php

namespace Api\Controller;

use App\Model;
use App\Storage\DataStorage;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
// todo: Route не используется, удалить.
use Symfony\Component\Routing\Annotation\Route;

class ProjectController 
{
    /**
     * @var DataStorage
     * todo: область видимости private на практике использовал крайне редко,
     *  очень сильно ограничивает наследование, добавляет кода и путает при чтении.
     *  Возможно в будущем от этого контроллера будет наследоваться другой контроллер,
     *  тогда ни этот контроллер не надо будет изменять, ни в наследнике вводить такую же/другую переменную.
     *  Поэтому вместо private использовал бы protected - protected $storage.
     *
     */
    private $storage;

    public function __construct(DataStorage $storage)
    {
        $this->storage = $storage;
    }

    /**
     * @param Request $request
     * // todo: по @Route и прочим переменным в комментариях ничего не скажу, не работал с ними.
     * @Route("/project/{id}", name="project", method="GET")
     */
    public function projectAction(Request $request)
    {
        try {
            $project = $this->storage->getProjectById($request->get('id'));

            return new Response($project->toJson());
        } catch (Model\NotFoundException $e) {
            return new Response('Not found', 404);
        } catch (\Throwable $e) {
            return new Response('Something went wrong', 500);
        }
    }

    /**
     * @param Request $request
     *
     * @Route("/project/{id}/tasks", name="project-tasks", method="GET")
     *
     * todo: в зависимости от клиента API'шки, поменял бы limit и offset на параметры page (номер страницы),
     *  onPage (количество записей на страницу), создал бы класс Paginator, который бы фильтровал ввод этих параметров
     *  и высчитывал limit и offset внутри себя. Было очень удобно, когда писал API для мобильного приложения - вся страничная
     *  логика в одном месте, а ещё в зависимости от роли пользователя можно ограничить количество выдаваемых записей на страницу,
     *  чтобы не повесить сервер при большом количестве записей.
     */
    public function projectTaskPagerAction(Request $request)
    {
        // todo: обернуть в try... catch, как в предыдущем методе, потому как тут тоже что-то может пойти не так.
        $tasks = $this->storage->getTasksByProjectId(
            $request->get('id'),
            $request->get('limit'),
            $request->get('offset')
        );

        return new Response(json_encode($tasks));
    }

    /**
     * @param Request $request
     * // todo: привести в соответствие с README.md (там метод не PUT, а POST)
     * @Route("/project/{id}/tasks", name="project-create-task", method="PUT")
     */
    public function projectCreateTaskAction(Request $request)
    {
      /* todo: если проект не будет найден, то будет выброшено исключение поэтому нужен блок try..catch.
       *  Отформатировать код, чтобы появились отступы.
       */
		$project = $this->storage->getProjectById($request->get('id'));
		if (!$project) {
			return new JsonResponse(['error' => 'Not found']);
		}

    // todo: $_REQUEST заменить на $_POST
		return new JsonResponse(
			$this->storage->createTask($_REQUEST, $project->getId())
		);
    }
}
